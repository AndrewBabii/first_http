import 'package:flutter/material.dart';

import 'models/pictures.dart';

class GalleryProvider extends ChangeNotifier {
  List<PictureItems> _pictureContent = new List<PictureItems>();

  List<PictureItems> get pictureContent {
    return [..._pictureContent];
  }

  void addPicture (List<PictureItems> items) {
    _pictureContent = items;
    notifyListeners();
  }
}