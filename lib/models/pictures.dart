import 'package:flutter/material.dart';

class PictureItems with ChangeNotifier {
  int id;
  String imageUrl, link, complexity, category;

  PictureItems({
    @required this.id,
    @required this.imageUrl,
  });

  factory PictureItems.fromJson(Map<String, dynamic> jsonData) => PictureItems(
        id: jsonData['id'],
        imageUrl: jsonData['urls'],
      );
}
