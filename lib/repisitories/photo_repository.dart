import 'dart:convert';

import 'package:first_http/gallery_provider.dart';
import 'package:first_http/models/pictures.dart';
import 'package:http/http.dart' as http;

class PhotoRepository {
  void getSearchResults() async {
    var response = await http.get(
        'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0');
    var jsonData = json.decode(utf8.decode(response.bodyBytes));

    getItems(jsonData);
  }

  List<PictureItems> getItems(var jsonData) {
    List<PictureItems> items = [];
    if (jsonData.length > 0) {
      for (int i = 0; i < jsonData['links'].lenght; i++) {
        items.add(
          PictureItems.fromJson(
            jsonData['links'][i],
          ),
        );
      }
    }
    if(items.isNotEmpty){
      Provider.of<GalleryProvider>().addPicture(items);
    }
  }
}